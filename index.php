<?php
  $myVar = "Ingrid ";
  echo "Hello " . $myVar;

  $myVar = $_GET["myVar"];
  echo "<br>Hello " . $myVar;

  $chiffre1 = $_GET["chiffre1"];
  $chiffre2 = $_GET["chiffre2"];
  $resultat = $chiffre1 * $chiffre2;
  echo "<br> Résultat: " . $resultat;

  if(isset($_GET["chiffre"])) {
    $chiffre = $_GET["chiffre"];
    if(($chiffre%2) == 0) {
        echo "<br>Le chiffre est pair";
    }
    else {
        echo "<br>Le chiffre est impair";
    }}
  else {
        echo "<br>Veuillez indiquer un chiffre";
    }

    $nbr = $_GET["nbr"];
    if($nbr < 10) {
        echo "<br>Lower than 10";
    }
    elseif ($nbr >= 10 && $nbr < 20) {
        echo "<br>Between 10 and 20";
    }
    else {
      echo "<br>Higher than 20";
    }

    for ($i=1; $i <=10; $i++) {
      echo "<br>line ".$i ;
    }

$names=array ("name 1", "name 2", "name 3", "name 4");
echo $names[1];
echo "<PRE>";
print_r ($names);
echo "</PRE>";

$members=array ("marco"=>"43", "nico"=>"29", "chris"=>"32", "thomas"=>"27");
echo "<PRE>";
print_r ($members);
echo "</PRE>";
echo ($members["marco"]);

$mem=array (
array ("name"=>"marco", "age"=>"42", "grade"=>"caporal"),
array ("name"=>"chris", "age"=>"32", "grade"=>"soldat"),
array ("name"=>"nico", "age"=>"27", "grade"=>"general")
);
echo "<PRE>";
print_r ($mem);
echo "</PRE>";

echo ($mem[1]["name"]." ".
      $mem[1]["age"]." ".
      $mem[1]["grade"]." ");

$people=array (
array ("name"=>"marco", "age"=>"42", "grade"=>"caporal"),
array ("name"=>"chris", "age"=>"32", "grade"=>"soldat"),
array ("name"=>"nico", "age"=>"27", "grade"=>"general"),
array ("name"=>"drew", "age"=>"42", "grade"=>"caporal"),
array ("name"=>"jackson", "age"=>"32", "grade"=>"soldat"),
array ("name"=>"adam", "age"=>"27", "grade"=>"general"),
array ("name"=>"thomas", "age"=>"42", "grade"=>"caporal"),
array ("name"=>"john", "age"=>"32", "grade"=>"soldat"),
array ("name"=>"jake", "age"=>"27", "grade"=>"general"),
array ("name"=>"brad", "age"=>"42", "grade"=>"caporal"),
);

echo "<ol>";
for ($i=0; $i <=9; $i++) {
echo       "<li>".
           ($people[$i]["name"]." ".
            $people[$i]["age"]." ".
            $people[$i]["grade"]." ").
            "</li>";
}
echo "</ol>";



 ?>
